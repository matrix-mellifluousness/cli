# CLI for Matrix APIs

This script provides a thin wrapper around `curl` to simplify the experience of interactively querying the [Matrix Client‑Server API](https://spec.matrix.org/v1.9/client-server-api/). [Synapse's Admin API](https://element-hq.github.io/synapse/latest/usage/administration/admin_api/index.html) is also supported.

[curl](https://curl.se/) and [jq](https://jqlang.github.io/jq/) must be installed to run this script. They can be found in virtually every Linux distribution's repositories.

JSON command output will always include syntax highlighting (a future update may change this behavior) — make sure your terminal emulator or other software reading command output supports [ANSI SGR control sequences](https://en.wikipedia.org/wiki/ANSI_escape_code#SGR_(Select_Graphic_Rendition)_parameters). If piping to less, this requires calling less with the `-R` option.

_Breaking changes will always be indicated by a bump in major version number, but a bump in major version number does not necessarily indicate breaking changes. Refer to the [release notes](https://gitlab.com/matrix-mellifluousness/cli/-/releases)._

## Usage

<dl><dt>

`. mmcli.sh ['homeserver base URL']`

</dt><dd>

Initializes the CLI for use, making available all other commands.

Prompts for the homeserver base URL if not specified on the command line (recommended) or as an environment variable `$HOMESERVER`. The homeserver base URL is the exact URL (and port if not 443) where the Matrix homeserver listens for the client‑server API, such as `https://matrix-client.matrix.org`. _It can be found in Element Settings > Help & About._

Also prompts for the access token to use. When the homeserver base URL is read from an environment variable, the access token may also be read from an environment variable `$ACCESS_TOKEN`. _It can be found in Element Settings > Help & About._

</dd><dt>

`request { GET | POST | PUT | DELETE } '/_matrix/api/v0/endpoint'`

</dt><dd>

Performs an arbitrary API call with specified request method to the specified endpoint. Except for GET, JSON request body is specified on standard input and may be empty. The response body is written to standard output, pretty‑printed if it is valid JSON.

</dd></dl>

### Supplying Standard Input

There are two ways to supply standard input: via pipe or interactively.

If nothing is piped in, you are prompted to interactively supply standard input after running the command. Send [End‑of‑Transmission](https://en.wikipedia.org/wiki/End-of-Transmission_character) when finished (typically <kbd>Ctrl+D</kbd> after a trailing newline). Do not use quotation marks except as part of the payload itself.

## Usage Examples

<dl><dt>

### Creating a private room

</dt><dd>

```shell
$ request POST '/_matrix/client/v3/createRoom'
{
  "preset": "private_chat"
}

```

```
Response:
{
  "room_id": "!pIYbArYMmUWepOXCQG:matrix.org"
}
```

</dd><dt>

### Creating a private room (pipe version)

</dt><dd>

```shell
$ echo '{ "preset": "private_chat" }' | request POST '/_matrix/client/v3/createRoom'
```

```
Response:
{
  "room_id": "!NlgAdszjyhGddFQNbA:matrix.org"
}
```

</dd><dt>

### Deleting an alias

</dt><dd>

```shell
$ request DELETE '/_matrix/client/v3/directory/room/%23matrix:matrix.org'

```

```
Response:
{
  "errcode": "M_FORBIDDEN",
  "error": "You don't have permission to delete the alias."
}
```

</dd><dt>

### Deleting an alias (pipe version)

</dt><dd>

```shell
$ echo | request DELETE '/_matrix/client/v3/directory/room/%23matrix:matrix.org'
```

```
Response:
{
  "errcode": "M_FORBIDDEN",
  "error": "You don't have permission to delete the alias."
}
```

</dd></dl>