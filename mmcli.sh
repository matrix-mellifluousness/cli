#!/bin/sh

# ====================
#
#  Matrix Mellifluousness CLI for Matrix APIs v2.0
#
#  This CLI supports version 1.9 of the Matrix Specification: https://spec.matrix.org/v1.9/
#
#  curl and jq must be installed to run this script. Refer to README.md for usage instructions.
#
# ====================

# Check dependencies
test -n "$(curl --version 2>&1 1> /dev/null)" && echo 'curl not found, aborting…' >&2 && return 1
test -n "$(jq --version 2>&1 1> /dev/null)" && echo 'jq not found, aborting…' >&2 && return 1

# Retrieve homeserver base URL and access token
mmcli_homeserver=
mmcli_access_token=
if [ -n "$1" ]; then
  mmcli_homeserver="$1"
elif [ -n "$HOMESERVER" ]; then
  mmcli_homeserver="$HOMESERVER"
  echo 'Homeserver base URL retrieved from environment variable.'
  # shellcheck disable=2153
  mmcli_access_token="$ACCESS_TOKEN"
else
  printf 'Please enter the base URL for your homeserver'\''s API: %b' '\033[4m'
  IFS= read -r mmcli_homeserver;
  printf '%b' '\033[0m'
fi
test -n "$mmcli_homeserver" || { echo 'Homeserver base URL not provided, aborting…' >&2 && return 1 ;}
# shellcheck disable=2015
test -n "$mmcli_access_token" && echo 'Access token retrieved from environment variable.' || { \
  printf 'Please enter your access token: %b' '\033[4m' \
  && IFS= read -r mmcli_access_token \
  ; printf '%b' '\033[0m\033[1F\033[32C\033[0J\n' \
  ;}
test -n "$mmcli_access_token" || { echo 'Access token not provided, aborting…' >&2 && return 1 ;}

# ====================

# request { GET | POST | PUT | DELETE } '/_matrix/api/v0/endpoint'
# Request body is read from stdin
request() {
  localvar_method="$1"
  localvar_endpoint="$2"
  test "$(printf '%s' "$localvar_endpoint" | cut -c1)" != '/' && localvar_endpoint="/$localvar_endpoint" # Add leading slash if not provided
  localvar_body=
  if [ "$localvar_method" = 'POST' ] || [ "$localvar_method" = 'PUT' ] || [ "$localvar_method" = 'DELETE' ]; then
    localvar_body="$(tee | jq -c)" || return 1 # Read from stdin
  elif [ "$localvar_method" != 'GET' ]; then
    echo 'Invalid request method, no request sent…' >&2 && return 1
  fi
  localvar_response="$(printf '%s' "$localvar_body" | curl -sS -X "$1" -H "Authorization: Bearer $mmcli_access_token" "$mmcli_homeserver$localvar_endpoint" --json @-)" || return 1
  printf '\n\e[1mResponse:\e[0m\n'
  { printf '%s' "$localvar_response" | jq -C 2> /dev/null ;} || { printf '%s' "$localvar_response" ;}
}
